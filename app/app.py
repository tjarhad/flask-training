from flask import Flask
from app.public.views import public_blueprint as public_blueprint
from app.auth.views import auth_blueprint
app = Flask(__name__)

app.register_blueprint(public_blueprint)
app.register_blueprint(auth_blueprint)

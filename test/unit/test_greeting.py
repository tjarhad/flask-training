from hamcrest import assert_that, is_

from app.User.model import User
from app.User.utils import get_greeting_user


class TestUserGreeting:
    def test_greeting_returns_user_name(self):
        user = User()
        user.name = 'John Watson'

        greeting = get_greeting_user(user)
        assert_that(greeting, is_('Hello John'))

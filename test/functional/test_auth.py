from hamcrest import assert_that, is_


class TestAuth:

    def test_login_successful(self, testapp):
        res = testapp.post_json('/auth/login', {'username': 'abc@gmail.com', 'password': '1234'})
        assert_that(res.status_code, is_(200))
        assert_that(res.json['message'], is_('success'))

    def test_login_unsuccessful(self, testapp):
        res = testapp.post_json('/auth/login', {'username': 'abc@gmail.com', 'password': '1235'}, expect_errors=True)
        assert_that(res.status_code, is_(401))
        assert_that(res.json['message'], is_('Password is wrong'))

